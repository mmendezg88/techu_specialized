(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'home': '/',
      'second':'/secondpage',
      'third':'/thirdpage'
    }
  });
}(document));
