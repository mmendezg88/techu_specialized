class DpProjectCellsEntrega extends Polymer.Element {

  static get is() {
    return 'dp-project-cells-entrega';
  }

  static get properties() {
    return {
     
    };
  }

  processData(params){
    let respuesta = JSON.stringify(params);
    let respuesta2 = JSON.parse(respuesta);
    console.log('BODY::::::::::'+JSON.stringify(respuesta2.results[0].picture.large));
    let image = respuesta2.results[0].picture.thumbnail;
    let email = respuesta2.results[0].email;
    let num = 1;
    this.dispatchEvent(new CustomEvent("data-image-api",{
      bubbles: true,
      composed: true, 
      detail:image
    }));
    this.dispatchEvent(new CustomEvent("data-header-api",{
      bubbles: true,
      composed: true, 
      detail:email
    }));
    this.dispatchEvent(new CustomEvent("data-flag-api",{
      bubbles: true,
      composed: true, 
      detail:num
    }));
  }
}

customElements.define(DpProjectCellsEntrega.is, DpProjectCellsEntrega);
