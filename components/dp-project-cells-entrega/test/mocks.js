const datosmocks= {
    results: [
      {
        gender: 'male',
        name: {
          title: 'monsieur',
          first: 'niklas',
          last: 'fontai'
        },
        location: {
          street: '5613 place de labbé-basset',
          city: 'pont-la-ville',
          state: 'vaud',
          postcode: 1608,
          coordinates: {
            latitude: '-53.2345',
            longitude: '-75.8278'
          },
          timezone: {
            offset: '-2:00',
            description: 'Mid-Atlantic'
          }
        },
        email: 'niklas.fontai@example.com',
        login: {
          uuid: 'b5683ae7-e779-4ff1-b4e9-ee191d171d15',
          username: 'tinymeercat933',
          password: 'dolphin1',
          salt: '5BBM77hT',
          md5: '45549301c271f0f8222a7cac27022169',
          sha1: '31499daaaa8d02a96595d9c76e485cd104ddc90c',
          sha256: '37230071f27b74998b7791b313ee291d09ff1fd6f2bd2ecfae9642ff444581f9'
        },
        dob: {
          date: '1992-12-24T22:34:33Z',
          age: 26
        },
        registered: {
          date: '2003-10-19T19:19:56Z',
          age: 15
        },
        phone: '(804)-180-3481',
        cell: '(316)-759-4254',
        id: {
          name: 'AVS',
          value: '756.2209.0355.39'
        },
        picture: {
          large: 'https://randomuser.me/api/portraits/men/61.jpg',
          medium: 'https://randomuser.me/api/portraits/med/men/61.jpg',
          thumbnail: 'https://randomuser.me/api/portraits/thumb/men/61.jpg'
        },
        nat: 'CH'
      }
    ],
    info: {
      seed: '981526980c1a4baf',
      results: 1,
      page: 1,
      version: '1.2'
    }
  }