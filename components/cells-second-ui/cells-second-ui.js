{
  const {
    html,
  } = Polymer;
  /**
    `<cells-second-ui>` Description.

    Example:

    ```html
    <cells-second-ui></cells-second-ui>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-second-ui | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsSecondUi extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'cells-second-ui';
    }

    static get properties() {
      return {
        imageUrl:String
      };
    }

    processData(params){
      let respuesta = JSON.stringify(params);
      let respuesta2 = JSON.parse(respuesta);
      console.log('BODY::::::::::'+JSON.stringify(respuesta2.results[0].picture.large));
      let image = respuesta2.results[0].picture.thumbnail;
      let email = respuesta2.results[0].email;
      let num = 1;
      this.dispatchEvent(new CustomEvent("data-image-api",{
        bubbles: true,
        composed: true, 
        detail:image
      }));
      this.dispatchEvent(new CustomEvent("data-header-api",{
        bubbles: true,
        composed: true, 
        detail:email
      }));
      this.dispatchEvent(new CustomEvent("data-flag-api",{
        bubbles: true,
        composed: true, 
        detail:num
      }));
    }

    }

  customElements.define(CellsSecondUi.is, CellsSecondUi);
}