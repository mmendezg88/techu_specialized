const KEYS = {
  SPACE: 32,
  ENTER: 13,
};
/**
 *
 *
 * # cells-radio-tabs
 *
 * ![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)
 * ![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)
 *
 * [Demo of component in Cells Catalog](https://catalogs.platform.bbva.com/cells)
 *
 * `<cells-radio-tabs>` displays a list of tabs from its `options` property.
 * The `options` property can be an array of strings or an array of objects with an optional `icon` property and a `label` property that will be used as the tab text.
 *
 * Example with simple options:
 *
 * ```html
 * <cells-radio-tabs options='["Home", "Accounts"]'></cells-radio-tabs>
 * ```
 *
 * Example with text and icons:
 *
 * ```html
 * <cells-radio-tabs options='[{
 *   "icon": "coronita:home",
 *   "label": "Home"
 * }, {
 *   "icon": "coronita:alarm",
 *   "label": "Alerts"
 * }]'></cells-radio-tabs>
 * ```
 *
 * ## Icons
 *
 * Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html). In fact, this component uses an iconset in its demo.
 *
 * ## Styling
 *
 * The following custom properties and mixins are available for styling:
 *
 * | Custom Property | Description | Default |
 * | :-------------- | :---------- | :------ |
 * | --cells-radio-tabs-border-radius | border-radius applied to host | 0 |
 * | --cells-radio-tabs-color | Text color | var(--bbva-300, #D3D3D3) |
 * | --cells-radio-tabs-item-height | Tab height | 3.4375rem (55px) |
 * | --cells-radio-tabs-bg-color | background-color applied to host | var(--bbva-white, #fff) |
 * | --cells-radio-tabs | Mixin applied to :host | {} |
 * | --cells-radio-tabs-option-flex | `flex` property applied to each tab | 1 |
 * | --cells-radio-tabs-border-bottom-color | border-bottom color for each tab | var(--bbva-200, #E9E9E9) |
 * | --cells-radio-tabs-option | Empty mixin for each tab | {} |
 * | --cells-radio-tabs-color-selected | Text color for selected tab | var(--bbva-core-blue, #004481) |
 * | --cells-radio-tabs-option-focus | Empty mixin applied to focused tabs | {} |
 * | --cells-radio-tabs-tab-hover | Empty mixin applied to tab on :hover | {} |
 * | --cells-radio-tabs-option-total | Empty mixin for tab total span | {} |
 * | --cells-radio-tabs-selected | Empty mixin for selected tab | {} |
 * | --cells-radio-tabs-content | Empty mixin for tab content (icon + text) | {} |
 * | --cells-radio-tabs-icon-margin | Margin applied to icon | 0 0.625rem 0 0 (0 10px 0 0) |
 * | --cells-radio-tabs-icon | Empty mixin for the icon | {} |
 * | --cells-radio-tabs-indicator-bg-color | background-color for the current tab indicator | var(--bbva-core-blue, #004481) |
 * | --cells-radio-tabs-round | Empty mixin for host round-total class | {} |
 * | --cells-radio-tabs-round-color | Text color when host has round-total class | var(--bbva-500, #666666) |
 * | --cells-radio-tabs-round-border-bottom-color| border-bottom color for each tab when host has round-total class | var(--bbva-300, #D3D3D3) |
 * | --cells-radio-tabs-round-label | Empty mixin for host round-total label | {} |
 * | --cells-radio-tabs-round-label-order | flex order of label when host has round-total class | 2 |
 * | --cells-radio-tabs-round-label-font-size | font-size of label when host has round-total class | rem(15px) |
 * | --cells-radio-tabs-round-label-class | Empty mixin for host round-total label with label class | {} |
 * | --cells-radio-tabs-round-total-order | flex order of total when host has round-total class | 1 |
 * | --cells-radio-tabs-round-total-font-size | font-size of total when host has round-total class | rem(14px) |
 * | --cells-radio-tabs-round-total | Empty mixin for host round-total total | {} |
 * | --cells-radio-tabs-total-margin-right | margin-right of total when host has round-total class | rem(16px) |
 * | --cells-radio-tabs-total-margin-left| margin-left of total when host has round-total class | 0 |
 * | --cells-radio-tabs-round-total-color | color of total when host has round-total class | var(--bbva-white, #FFFFFF) |
 * | --cells-radio-tabs-round-total-background-color | background-color of total when host has round-total class | var(--bbva-500, #666666) |
 * | --cells-ratio-tabs-round-total-before | Empty mixin for host round-total total:before | {} |
 * | --cells-radio-tabs-round-selected-background-color | background-color of total for selected tab when host has round-total class | rem(14px) |
 * |--cells-radio-tabs-round-option-checked | Empty mixin for host round-total total when selected tab | {} |
 *
 * @polymer
 * @customElement
 * @summary Display a list of navigation tabs.
 * @extends {Polymer.Element}
 * @demo demo/index.html
 * @hero cells-radio-tabs.png
 */
class CellsRadioTabs extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior ], Polymer.Element) {

  static get is() {
    return 'cells-radio-tabs';
  }

  static get properties() {
    return {
      /**
       * List of options.
       * Can be an array of Strings used as labels for each tab or Objects with a "label" key and and optional "icon" key to display
       * an icon to the left of the text.
       * @type {Array}
       */
      options: {
        type: Array,
        observer: '_optionsChanged',
        value: function() {
          return [];
        },
      },

      /**
       * Size for the icons.
       */
      iconSize: {
        type: Number,
        value: 24,
      },

      /**
       * Index of the selected option.
       */
      selected: {
        type: Number,
        value: 0,
        notify: true,
      },

      _uniqueID: {
        type: Number,
        value: function() {
          return new Date().valueOf();
        },
      },

      _hasLabels: {
        type: Boolean,
        computed: '_computeHasLabels(options)',
      },

      /**
       * Set to true to fire `tab-mouseevent` event when a tab receives mouseenter / mouseleave.
       */
      notifyMouseEvents: {
        type: Boolean,
        value: false,
      }
    };
  }

  static get observers() {
    return [
      '_selectedChanged(selected, options)'
    ];
  }

  _optionsChanged(newValue, previousValue) {
    if (previousValue && (previousValue.length !== newValue.length)) {
      this._updateTabStylesAfterResettingSelected();
    } else {
      this._setTabStyles();
    }
  }

  // Prevents changing the width and the position of the indicator at the same time.
  _updateTabStylesAfterResettingSelected() {
    this.classList.add('indicator-hidden');
    this._resetSelected()
      .then(() => {
        this._setTabStyles();
        this.classList.remove('indicator-hidden');
      });
  }

  _resetSelected() {
    return new Promise((resolve) => {
      const selectedDoesNotExist = this.options.indexOf(this.options[ this.selected ]) === -1;
      const timeout = (selectedDoesNotExist) ? 200 : 0;
      if (selectedDoesNotExist) {
        this.selected = 0;
      }
      setTimeout(resolve, timeout);
    });
  }

  _setTabStyles() {
    this.updateStyles({ '--radio-tabs-item-width': 100 / this.options.length + '%' });
    /**
     * Fired after updating the tab styles.
     * @event cells-radio-tabs-styles-updated
     * @param {Object} detail.itemWidth width applied to each tab
     */
    this.dispatchEvent(new CustomEvent('cells-radio-tabs-styles-updated', {
      bubbles: true,
      composed: true,
      detail: {
        itemWidth: 100 / this.options.length + '%'
      }
    }));
  }

  /**
   * Selects the item on click event and on keydown only if the key pressed is space or enter.
   * Prevents selecting an item while navigating through tabs using the tab key.
   */
  _setSelected(e) {
    if (e.target !== e.currentTarget) {
      return;
    }

    const keyShouldActiveTab = [KEYS.SPACE, KEYS.ENTER].indexOf(e.keyCode) !== -1;
    if (e.type !== 'keydown' || keyShouldActiveTab) {
      this.selected = e.model.index;
      /**
      * Fired when tab was selected
      * @event selected-tab
      */
      this.dispatchEvent(new CustomEvent('selected-tab', {
        detail: this.selected,
        bubbles: true,
        composed: true
      }));
    }
  }

  _computeChecked(selected, index) {
    return Number(selected) === index;
  }

  _selectedChanged(selected, options) {
    const selectedItemExists = options.indexOf(options[ selected ]) !== -1;
    if (selectedItemExists) {
      this.$.indicator.style.transform = `translateX(calc(100% * ${selected}))`;
    }
  }

  _computeHasLabels(options) {
    return options.some(option => option.label);
  }

  _onMouseEvent(e) {
    /**
     * Fired on tab mouseenter / mouseleave
     * @event tab-mouseevent
     * @param {Number} 'index' Index of the event.target tab
     * @param {String} 'type' Event type (mouseenter | mouseleave)
     */
    if (this.notifyMouseEvents) {
      this.dispatchEvent(new CustomEvent('tab-mouseevent', {
        bubbles: false,
        composed: true,
        detail: {
          index: e.model.index,
          type: e.type,
        },
      }));
    }
  }
}
customElements.define(CellsRadioTabs.is, CellsRadioTabs);
