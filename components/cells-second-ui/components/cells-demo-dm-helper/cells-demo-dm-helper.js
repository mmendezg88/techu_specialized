(function() {
  'use strict';

  Polymer({

    is: 'cells-demo-dm-helper',

    properties: {
      /**
       * Endpoint where the requests will be made.
       * @default Artichoke (https://artichoke.platform.bbva.com)
       */
      host: {
        type: String,
        value: 'https://artichoke.platform.bbva.com',
      },

      /**
       * Application type.
       */
      consumerId: {
        type: String,
        value: '13000013',
      },

      /**
       * User credentials.
       */
      user: {
        type: Object,
        value: function() {
          return {
            userId: 'P52340677A',
            password: '112233',
          };
        },
      },

      /**
       * For statics demos where it is not necessary to be logged.
       */
      withoutLogin: {
        type: Boolean,
        value: false,
      },

      /**
       * Login status.
       */
      logged: {
        type: Boolean,
        value: false,
        readOnly: true,
        notify: true,
      },

      /**
       * Data Manager methods.
       * Each method must be an object with the following properties:<br>
       * `event`: Name of the event that will be fired after clicking the method button.<br>
       * `label`: Label used in the button.<br>
       * `method` (optional): Name of the method. Example: `getAccounts()`<br>
       */
      dmMethods: {
        type: Array,
      },

      /**
       * Set to true to display a loading icon in the method buttons.
       */
      loadingData: {
        type: Boolean,
        value: false,
      },

      /**
       * Object with the response data that will be printed in the JSON viewer.
       */
      jsonData: {
        type: Object,
        notify: true,
        observer: '_jsonDataChanged',
        value: function() {
          return {};
        },
      },

      /**
       * Path to a CSS file with custom styles for the JSON viewer.
       */
      jsonEditorCustomStyles: {
        type: String,
        value: '../cells-demo-dm-helper/juicy-jsoneditor-custom-styles.css',
      },

      _loginFailed: {
        type: Boolean,
        value: false,
      },

      _notLoggedYet: {
        type: Boolean,
        value: true,
      },
    },

    /**
     * Last DM button clicked used to set the loading icon
     */
    _activeMethod: null,

    _login: function(e) {
      if (e.detail.value) {
        this.$.loginDm.login();
      }
    },

    _onLoginSuccess: function() {
      this._notLoggedYet = false;
      this._loginFailed = false;
      this._setLogged(true);
    },

    _onLoginFailure: function(e) {
      this._notLoggedYet = false;
      this._loginFailed = true;
      this._setLogged(false);
    },

    /**
     * Fired after clicking a Data Manager method button
     * @event event.model.item.event (event name set in dmMethods)
     */
    _onDmButtonClick: function(e) {
      this._activeMethod = e.model.item;
      this.dispatchEvent(new CustomEvent(e.model.item.event));
    },

    _computeHiddenIcon: function(item, loadingData) {
      return loadingData && this._activeMethod === item;
    },

    _jsonDataChanged: function(jsonData) {
      if ((jsonData) && (Object.keys(jsonData).length)) {
        this.async(function() {
          this.$.jsonView.editor.expandAll();
        }, 1);
      }
    },

    _computeDisabled: function(logged, withoutLogin) {
      if (withoutLogin && this.dmMethods) {
        return false;
      }
      if (logged) {
        return false;
      }
      return true;
    },

    _setHostClass: function(e) {
      this.classList.toggle('resizing', e.detail.state === 'start');
    },
  });
}());
