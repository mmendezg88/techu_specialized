let sharedPanel = null;
const classNames = obj => {
  return Object.keys(obj).filter(k => obj[ k ]).join(' ');
};
/**
 * ![cells-app-drawer screenshot](hero.svg)
 *
 * # cells-app-drawer
 *
 * ![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg) ![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)
 *
 * [Demo of component in Cells Catalog](https://catalogs.platform.bbva.com/cells)
 *
 * Material design: [Navigation drawer](https://www.google.com/design/spec/patterns/navigation-drawer.html)
 *
 * `<cells-app-drawer>` contains a drawer panel and a main panel.  The drawer
 * and the main panel are side-by-side with drawer on the left.  When the browser
 * window size is smaller than the `responsiveWidth`, `cells-app-drawer`
 * changes to narrow layout.  In narrow layout, the drawer will be stacked on top
 * of the main panel.  The drawer will slide in/out to hide/reveal the main
 * panel.
 *
 * Use the attribute `drawer` to indicate that the element is the drawer panel and
 * `main` to indicate that the element is the main panel.
 *
 * Example:
 * ```html
 * <cells-app-drawer>
 *   <div slot="drawer"> Drawer panel... </div>
 *   <div slot="main"> Main panel... </div>
 * </cells-app-drawer>
 * ```
 *
 * The drawer and the main panels are not scrollable.  You can set CSS overflow
 * property on the elements to make them scrollable or use `paper-header-panel`.
 *
 * Example:
 * ```html
 * <cells-app-drawer>
 *   <paper-header-panel slot="drawer">
 *     <paper-toolbar></paper-toolbar>
 *     <div> Drawer content... </div>
 *   </paper-header-panel>
 *   <paper-header-panel slot="main">
 *     <paper-toolbar></paper-toolbar>
 *     <div> Main content... </div>
 *   </paper-header-panel>
 * </cells-app-drawer>
 * ```
 *
 * An element that should toggle the drawer will automatically do so if it's
 * given the `paper-drawer-toggle` attribute.  Also this element will automatically
 * be hidden in wide layout.
 *
 * Example:
 * ```html
 * <cells-app-drawer>
 *   <paper-header-panel slot="drawer">
 *     <paper-toolbar>
 *       <div>Application</div>
 *     </paper-toolbar>
 *     <div> Drawer content... </div>
 *   </paper-header-panel>
 *   <paper-header-panel slot="main">
 *     <paper-toolbar>
 *       <paper-icon-button icon="menu" paper-drawer-toggle></paper-icon-button>
 *       <div>Title</div>
 *     </paper-toolbar>
 *     <div> Main content... </div>
 *   </paper-header-panel>
 * </cells-app-drawer>
 * ```
 *
 * To position the drawer to the right, add `right-drawer` attribute.
 *
 * ```html
 * <cells-app-drawer right-drawer>
 *   <div slot="drawer"> Drawer panel... </div>
 *   <div slot="main"> Main panel... </div>
 * </cells-app-drawer>
 * ```
 *
 * ## Styling
 *
 * To change the main container:
 *
 * ```css
 * cells-app-drawer {
 *   --cells-app-drawer-main-container: {
 *     background-color: gray;
 *   };
 * }
 * ```
 *
 * To change the drawer container when it's in the left side:
 *
 * ```css
 * cells-app-drawer {
 *   --cells-app-drawer-left-drawer-container: {
 *     background-color: white;
 *   };
 * }
 * ```
 *
 * To change the drawer container when it's in the right side:
 *
 * ```css
 * cells-app-drawer {
 *   --cells-app-drawer-right-drawer-container: {
 *     background-color: white;
 *   };
 * }
 * ```
 *
 * To customize the scrim:
 *
 * ```css
 * cells-app-drawer {
 *   --cells-app-drawer-scrim: {
 *     background-color: red;
 *   };
 * }
 * ```
 *
 * The following custom properties and mixins are available for styling:
 *
 * | Custom property | Description | Default |
 * | --- | --- | --- |
 * | `--cells-app-drawer-scrim-opacity` | Scrim opacity | 1 |
 * | `--cells-app-drawer-drawer-container` | Mixin applied to drawer container | {} |
 * | `--cells-app-drawer-left-drawer-container` | Mixin applied to container when it's in the left side | {} |
 * | `--cells-app-drawer-main-container` | Mixin applied to main container | {} |
 * | `--cells-app-drawer-right-drawer-container` | Mixin applied to container when it's in the right side | {} |
 * | `--cells-app-drawer-scrim` | Mixin applied to scrim | {} |
 *
 * @polymer
 * @customElement
 * @summary Contains a drawer panel and a main panel.
 * @extends {Polymer.Element}
 * @group Paper elements
 * @element cells-app-drawer
 * @demo demo/index.html
 * @hero hero.svg
 */
class CellsAppDrawer extends Polymer.mixinBehaviors([Polymer.IronResizableBehavior, Polymer.GestureEventListeners], Polymer.Element) {
  static get is() {
    return 'cells-app-drawer';
  }

  static get properties() {
    return {
      /**
       * The panel to be selected when `cells-app-drawer` changes to narrow
       * layout.
       */
      defaultSelected: {
        type: String,
        value: 'main'
      },
      /**
       * If true, swipe from the edge is disabled.
       */
      disableEdgeSwipe: {
        type: Boolean,
        value: false
      },
      /**
       * If true, swipe to open/close the drawer is disabled.
       */
      disableSwipe: {
        type: Boolean,
        value: false
      },
      /**
       * Whether the user is dragging the drawer interactively.
       */
      dragging: {
        type: Boolean,
        value: false,
        readOnly: true,
        notify: true
      },
      /**
       * Width of the drawer panel.
       */
      drawerWidth: {
        type: String,
        value: '100%'
      },
      /**
       * How many pixels on the side of the screen are sensitive to edge
       * swipes and peek.
       */
      edgeSwipeSensitivity: {
        type: Number,
        value: 40
      },
      /**
       * If true, ignore `responsiveWidth` setting and force the narrow layout.
       */
      forceNarrow: {
        type: Boolean,
        value: false
      },
      /**
       * Whether the browser has support for the transform CSS property.
       */
      hasTransform: {
        type: Boolean,
        value: function() {
          return 'transform' in this.style;
        }
      },
      /**
       * Whether the browser has support for the will-change CSS property.
       */
      hasWillChange: {
        type: Boolean,
        value: function() {
          return 'willChange' in this.style;
        }
      },
      /**
       * Returns true if the panel is in narrow layout.  This is useful if you
       * need to show/hide elements based on the layout.
       */
      narrow: {
        reflectToAttribute: true,
        type: Boolean,
        value: false,
        readOnly: true,
        notify: true
      },

      /**
       * Enable the drawer to peek out from the edge.
       */
      peek: {
        type: Boolean,
        value: false
      },
      /**
       * Whether the drawer is peeking out from the edge.
       */
      peeking: {
        type: Boolean,
        value: false,
        readOnly: true,
        notify: true
      },
      /**
       * Max-width when the panel changes to narrow layout.
       */
      responsiveWidth: {
        type: String,
        value: '768px'
      },
      /**
       * If true, position the drawer to the right.
       */
      rightDrawer: {
        type: Boolean,
        value: false
      },
      /**
       * The panel that is being selected. `drawer` for the drawer panel and
       * `main` for the main panel.
       *
       * @type {string|null}
       */
      selected: {
        reflectToAttribute: true,
        notify: true,
        type: String,
        value: null
      },

      scaleContent: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      },
      /**
       * The attribute on elements that should toggle the drawer on tap, also elements will
       * automatically be hidden in wide layout.
       */
      drawerToggleAttribute: {
        type: String,
        value: 'paper-drawer-toggle'
      },
      /**
       * The CSS selector for the element that should receive focus when the drawer is open.
       * By default, when the drawer opens, it focuses the first tabbable element. That is,
       * the first element that can receive focus.
       *
       * To disable this behavior, you can set `drawerFocusSelector` to `null` or an empty string.
       *
       */
      drawerFocusSelector: {
        type: String,
        value: 'a[href]:not([tabindex="-1"]),' +
          'area[href]:not([tabindex="-1"]),' +
          'input:not([disabled]):not([tabindex="-1"]),' +
          'select:not([disabled]):not([tabindex="-1"]),' +
          'textarea:not([disabled]):not([tabindex="-1"]),' +
          'button:not([disabled]):not([tabindex="-1"]),' +
          'iframe:not([tabindex="-1"]),' +
          '[tabindex]:not([tabindex="-1"]),' +
          '[contentEditable=true]:not([tabindex="-1"])'
      },
      /**
       * Whether the transition is enabled.
       */
      _transition: {
        type: Boolean,
        value: false
      }
    };
  }

  static get observers() {
    return [
      '_forceNarrowChanged(forceNarrow, defaultSelected)',
      '_toggleFocusListener(selected)'
    ];
  }

  ready() {
    Polymer.Gestures.addListener(this, 'tap', e => this._onTap(e));
    Polymer.Gestures.addListener(this, 'down', e => this._downHandler(e));
    Polymer.Gestures.addListener(this, 'up', e => this._upHandler(e));
    Polymer.Gestures.addListener(this, 'track', e => this._onTrack(e));
    super.ready();
    this._transition = true;
  }

  /**
   * Toggles the panel open and closed.
   *
   * @method togglePanel
   */
  togglePanel() {
    if (this._isMainSelected()) {
      this.openDrawer();
    } else {
      this.closeDrawer();
    }
  }

  /**
   * Opens the drawer.
   *
   * @method openDrawer
   */
  openDrawer() {
    requestAnimationFrame(function() {
      this.toggleClass('transition-drawer', true, this.$.drawer);
      this.selected = 'drawer';
    }.bind(this));
  }

  /**
   * Closes the drawer.
   *
   * @method closeDrawer
   */
  closeDrawer() {
    requestAnimationFrame(function() {
      this.toggleClass('transition-drawer', true, this.$.drawer);
      this.selected = 'main';
    }.bind(this));
  }

  _onTransitionEnd(e) {
    var target = e.target;
    if (target !== this) {
      return;
    }
    if (e.propertyName === 'left' || e.propertyName === 'right') {
      this.notifyResize();
    }
    if (e.propertyName === 'transform') {
      requestAnimationFrame(function() {
        this.toggleClass('transition-drawer', false, this.$.drawer);
      }.bind(this));
      if (this.selected === 'drawer') {
        setTimeout(function() {
          var focusedChild = this._getAutoFocusedNode();
          if (focusedChild && focusedChild.focus()) {
            focusedChild.focus();
          }
        }.bind(this), 116);
      }
    }
  }

  _computeIronSelectorClass(narrow, transition, dragging, rightDrawer, peeking) {
    return classNames({
      dragging: dragging,
      'narrow-layout': narrow,
      'right-drawer': rightDrawer,
      'left-drawer': !rightDrawer,
      transition: transition,
      peeking: peeking
    });
  }

  _computeDrawerStyle(drawerWidth) {
    return 'width:' + drawerWidth + ';';
  }

  _computeMainStyle(narrow, rightDrawer, drawerWidth) {
    var style = '';
    style += 'left:' + (narrow || rightDrawer ? '0' : drawerWidth) + ';';
    if (rightDrawer) {
      style += 'right:' + (narrow ? '' : drawerWidth) + ';';
    }
    return style;
  }

  _computeMediaQuery(forceNarrow, responsiveWidth) {
    return forceNarrow ? '' : '(max-width: ' + responsiveWidth + ')';
  }

  _computeSwipeOverlayHidden(narrow, disableEdgeSwipe) {
    return !narrow || disableEdgeSwipe;
  }

  _onTrack(event) {
    if (!(event && event.detail && event.detail.state)) {
      return;
    }

    if (sharedPanel && this !== sharedPanel) {
      return;
    }

    switch (event.detail.state) {
      case 'start':
        this._trackStart(event);
        break;
      case 'track':
        this._trackX(event);
        break;
      case 'end':
        this._trackEnd(event);
        break;
    }
  }

  /**
   * Fired when the narrow layout changes.
   *
   * @event cells-app-responsive-change {{narrow: boolean}} detail -
   *     narrow: true if the panel is in narrow layout.
   */
  _responsiveChange(narrow) {
    this._setNarrow(narrow);
    this.selected = this.narrow ? this.defaultSelected : null;
    this.setScrollDirection(this._swipeAllowed() ? 'y' : 'all');
    this.dispatchEvent(new CustomEvent('cells-app-responsive-change', {
      detail: {
        narrow: this.narrow
      },
      bubbles: true,
      composed: true
    }));
  }

  _onQueryMatchesChanged(event) {
    this._responsiveChange(event.detail.value);
  }

  _forceNarrowChanged() {
    this._responsiveChange(this.forceNarrow || this.$.mq.queryMatches);
  }

  _swipeAllowed() {
    return this.narrow && !this.disableSwipe;
  }

  _isMainSelected() {
    return this.selected === 'main';
  }

  _startEdgePeek() {
    this.width = this.$.drawer.offsetWidth;
    this._moveDrawer(this._translateXForDeltaX(this.rightDrawer ? -this.edgeSwipeSensitivity : this.edgeSwipeSensitivity));
    this._setPeeking(true);
  }

  _stopEdgePeek() {
    if (this.peeking) {
      this._setPeeking(false);
      this._moveDrawer(null);
    }
  }

  _downHandler(event) {
    if (!this.dragging && this._isMainSelected() && this._isEdgeTouch(event) && !sharedPanel && this.peek) {
      this._startEdgePeek();
      event.preventDefault();
      /* eslint-disable consistent-this */
      sharedPanel = this;
      /* eslint-enable consistent-this */
    }
  }

  _upHandler() {
    this._stopEdgePeek();
    sharedPanel = null;
  }

  _onTap(event) {
    var targetElement = event.target;
    var isTargetToggleElement = targetElement && this.drawerToggleAttribute && targetElement.hasAttribute(this.drawerToggleAttribute);
    if (isTargetToggleElement) {
      this.togglePanel();
    }
  }

  _isEdgeTouch(event) {
    var x = event.detail.x;
    return !this.disableEdgeSwipe && this._swipeAllowed() && (this.rightDrawer ? x >= this.offsetWidth - this.edgeSwipeSensitivity : x <= this.edgeSwipeSensitivity);
  }

  _trackStart(event) {
    if (this._swipeAllowed()) {
      /* eslint-disable consistent-this */
      sharedPanel = this;
      /* eslint-enable consistent-this */
      this._setDragging(true);
      if (this._isMainSelected()) {
        this._setDragging(this.peeking || this._isEdgeTouch(event));
      }
      if (this.dragging) {
        this.width = this.$.drawer.offsetWidth;
        this._transition = false;
      }
    }
  }

  _translateXForDeltaX(deltaX) {
    var isMain = this._isMainSelected();
    if (this.rightDrawer) {
      return Math.max(0, isMain ? this.width + deltaX : deltaX);
    } else {
      return Math.min(0, isMain ? deltaX - this.width : deltaX);
    }
  }

  _trackX(event) {
    if (this.dragging) {
      var dx = event.detail.dx;
      if (this.peeking) {
        if (Math.abs(dx) <= this.edgeSwipeSensitivity) {
          return;
        }
        this._setPeeking(false);
      }
      this._moveDrawer(this._translateXForDeltaX(dx));
    }
  }

  _trackEnd(event) {
    if (this.dragging) {
      var xDirection = event.detail.dx > 0;
      this._setDragging(false);
      this._transition = true;
      sharedPanel = null;
      this._moveDrawer(null);
      if (this.rightDrawer) {
        this[ xDirection ? 'closeDrawer' : 'openDrawer' ]();
      } else {
        this[ xDirection ? 'openDrawer' : 'closeDrawer' ]();
      }
    }
  }

  _transformForTranslateX(translateX) {

    if (translateX === null) {
      return '';
    }
    let left;
    let scale;
    if (this.rightDrawer) {
      left = translateX * (-95 / this.width) + 95;
      scale = translateX * ((1 - 0.66) / this.width) + 0.66;
    } else {
      left = translateX * (-95 / -this.width) + 95;
      scale = translateX * ((1 - 0.66) / -this.width) + 0.66;
    }
    if (this.scaleContent) {
      if (this.rightDrawer) {
        return this.hasWillChange ? 'scale(' + scale + ') translateX(-' + left + 'vw)' : 'scale(' + scale + ') translate3d(' + left + 'vw, 0, 0)';
      } else {
        return this.hasWillChange ? 'scale(' + scale + ') translateX(' + left + 'vw)' : 'scale(' + scale + ') translate3d(' + left + 'vw, 0, 0)';
      }
    }

    return this.hasWillChange ? 'translateX(' + translateX + 'px)' : 'translate3d(' + translateX + 'px, 0, 0)';
  }

  _moveDrawer(translateX) {
    var panel = this.scaleContent ? this.$.main : this.$.drawer;
    this.transform(this._transformForTranslateX(translateX), panel);
  }

  _getDrawerContent() {
    return this.shadowRoot
      .querySelector('#drawerContent')
      .assignedNodes({
        flatten: true
      })
      .filter(n => n.nodeType === Node.ELEMENT_NODE)[ 0 ];
  }

  _getAutoFocusedNode() {
    var drawerContent = this._getDrawerContent();
    return this.drawerFocusSelector ? drawerContent.querySelector(this.drawerFocusSelector) || drawerContent : null;
  }

  _toggleFocusListener(selected) {
    if (selected === 'drawer') {
      this.addEventListener('focus', this._didFocus, true);
    } else {
      this.removeEventListener('focus', this._didFocus, true);
    }
  }

  _didFocus(event) {
    var autoFocusedNode = this._getAutoFocusedNode();
    if (!autoFocusedNode) {
      return;
    }
    var path = event.composedPath();
    var focusedChild = path[ 0 ];
    var drawerContent = this._getDrawerContent();
    var focusedChildCameFromDrawer = path.indexOf(drawerContent) !== -1;
    if (!focusedChildCameFromDrawer) {
      event.stopPropagation();
      autoFocusedNode.focus();
    }
  }

  _isDrawerClosed(narrow, selected) {
    return !narrow || selected !== 'drawer';
  }

  /**
   * Fired when the a panel is selected.
   *
   * Listening for this event is an alternative to observing changes in the `selected` attribute.
   * This event is fired both when a panel is selected.
   *
   * @event iron-select {{item: Object}} detail -
   *     item: The panel that the event refers to.
   */

  /**
   * Fired when a panel is deselected.
   *
   * Listening for this event is an alternative to observing changes in the `selected` attribute.
   * This event is fired both when a panel is deselected.
   *
   * @event iron-deselect {{item: Object}} detail -
   *     item: The panel that the event refers to.
   */
}
window.customElements.define(CellsAppDrawer.is, CellsAppDrawer);
