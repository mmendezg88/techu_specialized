const cellsTemplateAnimationBehavior = CellsBehaviors.CellsTemplateAnimationBehavior;
/**
 * ## A set of zones elements for your template. It includes:
 *
 * `<cells-template-paper-drawer-panel>` contains a drawer panel and a main panel. The drawer and the main panel are side-by-side with drawer on the left.
 *
 *   ```app__header``` - A container element for top app.
 *
 *   ```app__main``` - A container element main content and general manager scroll.
 *
 *   ```app__footer``` - A container element for bottom app.
 *
 *   ```app__complementary``` - A container element for drawer. [```Navigation drawer*```](https://www.google.com/design/spec/patterns/navigation-drawer.html)
 *
 *   ```app__overlay``` - A container element for overlay all content.
 *
 *   ```app__transactional``` - A container element that positions transverse layers.
 *
 * [__*The navigation drawer slides in from the left and contains the navigation destinations for your app.__](https://www.google.com/design/spec/patterns/navigation-drawer.html)
 *
 * Example cells context: *(view demo for declarative app)*
 * ```html
 * <cells-template-paper-drawer-panel></cells-template-paper-drawer-panel>
 *
 * <cells-template-paper-drawer-panel right-drawer drawer-width="300px"></cells-template-paper-drawer-panel>
 * ```
 * ## Styling
 *
 * The following custom properties and mixins are available for styling:
 *
 * ### Custom Properties
 * | Custom Property                                          | Selector                                                       | CSS Property     | Value                                                    |
 * | -------------------------------------------------------- | -------------------------------------------------------------- | ---------------- | -------------------------------------------------------- |
 * | --cells-template-paper-drawer-panel-footer-height        | :host([footer-fixed]) > --cells-paper-scroll-header-container: | padding-bottom   |  60px                                                    |
 * | --cells-template-paper-drawer-panel-footer-bg-color      | :host([has-footer]) .app__footer                               | background-color |  ![#fff](https://placehold.it/15/fff/000000?text=+) #fff |
 * | --cells-template-paper-drawer-panel-footer-height        | :host([has-footer]) .app__footer                               | height           |  60px                                                    |
 * | --cells-template-paper-drawer-panel-section-zindex       | .app__section                                                  | z-index          |  0                                                       |
 * | --cells-template-paper-drawer-panel-section-bg           | .app__section                                                  | background-color |  ![#fff](https://placehold.it/15/fff/000000?text=+) #fff |
 * | --cells-template-paper-drawer-panel-header-zindex        | .app__header                                                   | z-index          |  0                                                       |
 * | --cells-template-paper-drawer-panel-main-padding-top     | .app__main                                                     | padding-top      |  0                                                       |
 * | --cells-template-paper-drawer-panel-main-padding-bottom  | .app__main                                                     | padding-bottom   |  0                                                       |
 * | --cells-template-paper-drawer-panel-complementary-width  | .app__complementary                                            | width            |  100%                                                    |
 * | --cells-template-paper-drawer-panel-complementary-zindex | .app__complementary                                            | z-index          |  0                                                       |
 * | --cells-template-paper-drawer-panel-overlay-zindex       | .app__overlay                                                  | z-index          |  0                                                       |
 * | --cells-template-paper-drawer-panel-transactional-zindex | .app__transactional                                            | z-index          |  0                                                       |
 * ### @apply
 * | Mixins                                               | Selector                                                       | Value |
 * | ---------------------------------------------------- | -------------------------------------------------------------- | ----- |
 * | --cells-template-paper-drawer-panel                  | :host                                                          | {}    |
 * | --cells-template-paper-drawer-panel-state-active     | :host([state="active"])                                        | {}    |
 * | --cells-template-paper-drawer-panel-has-footer       | :host([has-footer])                                            | {}    |
 * | --cells-template-paper-drawer-panel-footer-fixed     | :host([footer-fixed]) > --cells-paper-scroll-header-container: | {}    |
 * | --cells-template-paper-drawer-panel-footer           | :host([has-footer]) .app__footer                               | {}    |
 * | --cells-template-paper-drawer-panel-containersection | .app__section                                                  | {}    |
 * | --cells-template-paper-drawer-panel-section          | .app__section                                                  | {}    |
 * | --cells-template-paper-drawer-panel-header           | .app__header                                                   | {}    |
 * | --cells-template-paper-drawer-panel-relative-main    | :host[relative-main] .app__main                                | {}    |
 * | --cells-template-paper-drawer-panel-main             | .app__main                                                     | {}    |
 * | --cells-template-paper-drawer-panel-overflow-initial | :host(.overflow-initial)                                       | {}    |
 * | --cells-template-paper-drawer-panel-complementary    | .app__complementary                                            | {}    |
 * | --cells-template-paper-drawer-panel-overlay          | .app__overlay                                                  | {}    |
 * | --cells-template-paper-drawer-panel-transactional    | .app__transactional                                            | {}    |
 *
 * @polymer
 * @customElement
 * @summary Template with Polymer paper drawer panel.
 * @extends {Polymer.Element}
 * @demo demo/index.html
 * @hero cells-template-paper-drawer-panel.png
 * @composer
 * @description Template with Polymer __paper drawer panel__
 * @type template
 * @platforms android, desktop, ios
 * @family cells-catalog-templates-family
 */
class CellsTemplatePaperDrawerPanel extends cellsTemplateAnimationBehavior(Polymer.Element) {
  static get is() {
    return 'cells-template-paper-drawer-panel';
  }

  /**
   * Object describing property-related metadata used by Polymer features
   */
  static get properties() {
    return {

      /**
       * If true, position the drawer to the right.
       */
      rightDrawer: {
        type: Boolean,
        value: false,
      },

      /**
       * Hides the header.
       */
      headerHidden: {
        type: Boolean,
        value: false,
      },

      /**
      * If true, swipe to open/close the drawer is disabled.
      */
      disableEdgeSwipe: {
        type: Boolean,
        value: false,
      },

      /**
      * If true, swipe to open/close the drawer is disabled.
      */
      disableSwipe: {
        type: Boolean,
        value: false,
      },

      /**
       * How many pixels on the side of the screen are sensitive to edge swipes and peek.
       */
      edgeSwipeSensitivity: {
        type: Number,
        value: 20,
      },

      /**
       * Open menu with scale animation.
       */
      scaleContent: {
        type: Boolean,
        value: false,
      },

      /**
       * Choose add css attribute position:relative in main content ('false', or 'true').
       */
      relativeMain: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },

      /**
       * The width of the drawer.
       */
      drawerWidth: {
        type: String,
        value: '100%',
      },

      /**
       * If true, the header is fixed to the top and never moves away.
       */
      headerFixed: {
        type: Boolean,
        value: false,
      },

      /**
       * Selected panel: 'drawer' or 'main'.
       */
      selected: {
        type: String,
        value: null,
        notify: true,
        readOnly: true,
      },

      /**
       * Notifies if the menu is opened or not.
       */
      menuOpened: {
        type: Boolean,
        value: false,
        notify: true,
        readOnly: true,
        reflectToAttribute: true,
      },

      /**
       * Sets wheter to disable scrolling when the menu is opened.
       */
      disableScrollLock: {
        type: Boolean,
        value: false,
      },

      /**
       * Set to true to prevent reset the scroll in cached templates
       */
      disabledScrollerReset: {
        type: Boolean,
        value: false,
      },

      /**
       * Selector of the template element that has the scrolling area
       */
      scrollerNode: {
        type: String,
        value: 'app__section',
        observer: '_scrollerNodeChanged',
      },

      /**
       * HTML element responsible for managing the scroll
       */
      _scrollerNode: {
        type: Object,
      },

      /**
      * Notifies number of configurations items
      */
      configurationsNumber: {
        type: Number,
        value: 0,
        notify: true,
      },
      /**
       * Footer height in px.
       * Can be set either by using a custom CSS property (--app__footer-height) or by setting
       * footerHeight. Default value for --app__footer-height is 60px.
       */
      footerHeight: {
        type: Number,
        observer: '_updateFooterHeight',
      },
      /**
       * Set to true if the template has footer or coexists with a external footer
       */
      hasFooter: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
      /**
       * If true, the footer is fixed to the bottom and never moves away.
       */
      footerFixed: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
      /**
       * Indicate is app__confirm zone is opened
       * @type  {Boolean}
       */
      collapseOpened: {
        type: Boolean,
        value: false,
        observer: '_onConfirmOpened'
      }
    };
  }

  ready() {
    this.addEventListener('overlay-opening', this._removeOverflow.bind(this));
    this.addEventListener('overlay-closed', this._addOverflow.bind(this));
    this.addEventListener('toggle-menu', this.toggleMenu.bind(this));
    this.addEventListener('change-template-animation', this._onChangeAnimation.bind(this));
    this.setAttribute('data-template', '');
    super.ready();
  }

  static get observers() {
    return [
      '_setupFooter(hasFooter)',
    ];
  }

  /**
   * Open/closes the menu panel
   */
  toggleMenu() {
    this.$[ 'app-container' ].togglePanel();
  }

  /**
   * Toggles #app__confirm zone.
   *
   * @params  show {Boolean}
   * @private
   */
  showConfirm(opened) {
    this[opened ? 'openConfirm' : 'closeConfirm']();
  }

  /**
   * Open confirm zone
   */
  openConfirm() {
    this.$.app__confirm.show();
  }

  /**
   * Close confirm zone
   */
  closeConfirm() {
    this.$.app__confirm.hide();
  }

  _onConfirmOpened() {
    if (this._handleTaskId) {
      clearTimeout(this._handleTaskId);
    }
    this._handleTaskId = setTimeout(() => {
      this.$.app__confirm.scrollIntoView(false);
      this._handleTaskId = null;
    }, 400);
  }

  _onSelectedChanged(e) {
    this._setSelected(e.detail.value);

    if (e.detail.value === 'drawer') {
      this._setMenuOpened(true);
      if (!this.disableScrollLock) {
        this._removeOverflow();
      }
    } else {
      this._setMenuOpened(false);
      if (!this.disableScrollLock) {
        this._addOverflow();
      }
    }
  }

  /**
   * Add scroll bar
   */
  _addOverflow() {
    this.classList.remove('overflow-initial');
  }

  /**
   * Remove scroll bar
   */
  _removeOverflow() {
    this.classList.add('overflow-initial');
  }

  /**
   * Fired when get scroll node. Sends the scroller node.
   *
   * @event scroller-control
   */
  _scrollerNodeChanged(scrollerControl) {
    let scrollerControlNode = this.$[ scrollerControl ] ? this.$[ scrollerControl ].scroller : '';
    this.set('_scrollerNode', scrollerControlNode);
    this.dispatchEvent(new CustomEvent('scroller-control', {
      bubbles: true,
      composed: true,
      detail: scrollerControlNode,
    }));
  }

  _onChangeAnimation(ev) {
    ev.stopPropagation();
    this.animationType = ev.detail.animationType;
  }

  /**
   * Fired when the content has been scrolled.
   *
   * @event template-content-scroll
   */
  _onContentScroll(ev) {
    ev.stopPropagation();
    this.dispatchEvent(new CustomEvent('template-content-scroll', {
      bubbles: true,
      composed: true,
      detail: ev.detail,
    }));
  }

  _setupFooter(hasFooter) {
    if (hasFooter) {
      this.$.app__section.addEventListener('paper-header-transform', this._onHeaderTransform.bind(this));
      return;
    }
    this.$.app__section.removeEventListener('paper-header-transform', this._onHeaderTransform.bind(this));
    return;
  }

  _onHeaderTransform(e) {
    if (!this.footerHeight) {
      let y = e.detail.y || '';

      this._footer = this._footer || this.$.app__footer;
      this._footer.style = 'transform: translateY(' + Math.abs(y) + 'px);';
    }
  }

  _updateFooterHeight(footerHeight) {
    if (this.hasFooter) {
      this.updateStyles({ '--cells-template-paper-drawer-panel-footer-height': footerHeight + 'px' });
    }
  }
}
window.customElements.define(CellsTemplatePaperDrawerPanel.is, CellsTemplatePaperDrawerPanel);