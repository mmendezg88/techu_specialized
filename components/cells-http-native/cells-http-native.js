/**
 * 
 *
 * # cells-http-native
 *
 * ![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg) ![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)
 *
 * [Demo of component in Cells Catalog](https://catalogs.platform.bbva.com/cells)
 *
 * The `cells-http-native` element exposes network request functionality.
 * ```html
 * <cells-http-native
 *    url="http://gdata.youtube.com/feeds/api/videos/"
 *    params='{"alt":"json", "q":"chrome"}'
 *    on-response="handleResponse"></cells-http-native>
 * ```
 * You can trigger a request explicitly by calling `generateRequest` on the element.
 *
 * This component manages the native header
 * ## Dependencies
 *
 *  The component depends on the native plugin, you could see the url on the bower file.
 *
 * Example:
 * ```html
 * <cells-cordova-nativeheader></cells-cordova-nativeheader>
 * ```
 *
 * ## Methods
 *
 * #### addHeader
 * This method shows a native header and add a title
 *
 * ```js
 * component.addHeader({"title":"Bienvenido a BBVA"});
 * ```
 *
 * #### hideHeader
 * This method hide the native header
 *
 * ```js
 * component.hideHeader();
 * ```
 *
 * #### showHeader
 * This method shows the native header
 *
 * ```js
 * component.showHeader();
 * ```
 *
 * #### changeTitle
 * This method change the native header title
 *
 * ```js
 * component.changeTitle({"title":"Bienvenido a BBVA"});
 * ```
 *
 * #### showBackButton
 * This method shows the back button of the native header
 *
 * ```js
 * component.showBackButton();
 * ```
 *
 * #### showMenuButton
 * This method shows the menu button of the native header
 *
 * ```js
 * component.showMenuButton();
 * ```
 *
 * #### hideBackButton
 * This method hide the back button of the native header
 *
 * ```js
 * component.hideBackButton();
 * ```
 *
 *
 * #### hideMenuButton
 * This method hide the menu button of the native header
 *
 * ```js
 * component.hideMenuButton();
 * ```
 *
 * ### ReceiveMessage
 * You will receive the message action like the example below.
 * Actions are:
 *
 * - ``onBackClicked``
 * - ``onMenuClicked``
 * - ``onBackground``
 * - ``onForeground``
 *
 * ```js
 * receiveMessage: function(e) {
 *         console.log(e.origin);
 *         var data = JSON.parse(e.data);
 *         var pluginName = data.pluginName;
 *         var action = data.action;
 *         var origin = data.origin;
 *         console.log(e.data);
 *         document.getElementById("data").innerHTML = ";pluginName: "+pluginName+
 *         " ;action: "+action+" ;origin: "+origin;
 *       }
 * ```
 *
 * @polymer
 * @customElement
 * @summary Exposes network request functionality
 * @extends {Polymer.Element}
 * @demo demo/index.html
 * @hero hero.svg
 */
class CellsHttpNative extends Polymer.Element {
  static get is() {
    return 'cells-http-native';
  }
  static get properties() {
    return {
      headers: {
        type: Object,
        value: {}
      },
      params: {
        type: Object,
        value: {}
      },
      url: { type: String },
      method: {
        type: String,
        value: 'get'
      },
      multipart: {
        type: Object,
        value: {}
      },
      /**
       * The most recent request made by this cells-http-native element.
       */
      lastRequest: {
        type: Object,
        notify: true,
        readOnly: true
      },
      /**
       * True while lastRequest is in flight.
       */
      loading: {
        type: Boolean,
        notify: true,
        readOnly: true
      },
      /**
       * Will be set to the most recent response received by a request
       * that originated from this element.
       */
      lastResponse: {
        type: Object,
        notify: true,
        readOnly: true
      },
      /**
       * Will be set to the most recent error that resulted from a request
       * that originated from this element.
       */
      lastError: {
        type: Object,
        notify: true,
        readOnly: true
      }
    };
  }

  get isPluginManagerEnabled() {
    return !!window.CellsNativePlugins && !!window.CellsNativePlugins.Http;
  }

  get isCordovaHttpPluginEnabled() {
    return !!window.cordovaCells && !!window.cordovaCells.Http;
  }

  get hasHttpEngine() {
    return this.isPluginManagerEnabled || this.isCordovaHttpPluginEnabled;
  }

  /**
   * Performs an native request to the specified URL.
   *
   * @return {!IronRequestElement}
   */
  generateRequest() {
    if (!this.hasHttpEngine) {
      console.warn('There isnt any HTTP plugin initialized');
      return;
    }

    const request = this._buildRequest();

    return this._sendRequest(request);
  }

  _buildRequest() {
    const request = document.createElement('iron-request');

    request.completes.then(this._handleResponse.bind(this)).catch(this._handleError.bind(this, request)).then(this._discardRequest.bind(this, request));
    return request;
  }

  _sendRequest(request) {
    const requestOptions = this.toRequestOptions();

    if (this.isCordovaHttpPluginEnabled) {
      return this._sendRequestThroughCordova(request, requestOptions);
    } else if (this.isPluginManagerEnabled) {
      return this._sendRequestThroughPluginManager(request, requestOptions);
    }
  }

  _sendRequestThroughCordova(request, requestOptions) {
    const { method, url, params, headers, multipart } = requestOptions;

    window.cordovaCells.Http[method](url, params, headers, (msg) => this._handleRequestSuccess(request, msg), (msg) => this._handleRequestFail(request, msg), multipart);

    return request;
  }

  _sendRequestThroughPluginManager(request, requestOptions) {
    const { method, url, params, headers, multipart } = requestOptions;

    window.CellsNativePlugins.Http[method](url, params, headers, (msg) => this._handleRequestSuccess(request, msg), (msg) => this._handleRequestFail(request, msg), multipart);

    return request;
  }

   _handleRequestSuccess(request, msg) {
     let data = msg.data;
     if (this.checkIsAJson(msg.data)) {
       data = JSON.parse(data);
     } else {
       const parser = new DOMParser();
       data = parser.parseFromString(data, 'text/xml');
     }
     const status = msg.status ? JSON.parse(msg.status) : 0;
     const headers = msg.headers ? JSON.parse(msg.headers) : {};
     const response = {
       status,
       data,
       headers
     };
     const extendedResponse = Object.assign({}, response.data, { headers: response.headers });
     request._setResponse(extendedResponse);
     this._setLastResponse(response);
     request.resolveCompletes(request);
   }

   _handleRequestFail(request, msg) {
     this._setLastError(msg);
     request.rejectCompletes(msg);
   }

  /**
   * Check if 'str' is a valid JSON
   */
  checkIsAJson(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  /**
   * Request options suitable for generating an `iron-request` instance based
   * on the current state of the `cells-http-native` instance's properties.
   *
   * @return
   * ```json
   * {
   *   url: string,
   *   method: (string|undefined),
   *   headers: (Object|undefined),
   *   params: (string|undefined)
   * }
   * ```
   */
  toRequestOptions() {
    const requestOptions = {
      url: this.url,
      method: this.method.toLowerCase(),
      headers: this.headers,
      params: this.body ? this.body : this.params,
      multipart: this.multipart,
    };

    if (requestOptions.params === '') {
      requestOptions.params = {};
    }

    return requestOptions;
  }

  /**
   * Fired response event
   *
   * @event response
   */
  _handleResponse(request) {
    if (request === this.lastRequest) {
      this._setLastResponse(request.response);
      this._setLastError(null);
      this._setLoading(false);
    }
    this.dispatchEvent(new CustomEvent('response', {
      bubbles: true,
      composed: true,
      detail: request.response
    }));
  }

  /**
   * Fired error event
   *
   * @event error
   */
  _handleError(request, error) {
    if (this.verbose) {
      console.error(error);
    }
    if (request === this.lastRequest) {
      this._setLastError({
        request: request,
        error: error
      });
      this._setLastResponse(null);
      this._setLoading(false);
    }
    this.dispatchEvent(new CustomEvent('error', {
      bubbles: true,
      composed: true,
      detail: {
        request: request,
        error: error
      }
    }));
  }
  _discardRequest(request) {
    //TODO: nothing here for the moment...
  }
}
window.customElements.define(CellsHttpNative.is, CellsHttpNative);
