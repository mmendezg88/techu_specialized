![cells-http-native screenshot](native.svg)

# cells-http-native

![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg) ![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)

[Demo of component in Cells Catalog](https://catalogs.platform.bbva.com/cells)

The `cells-http-native` element exposes network request functionality.
```html
<cells-http-native
url="http://gdata.youtube.com/feeds/api/videos/"
params='{"alt":"json", "q":"chrome"}'
on-response="handleResponse"></cells-http-native>
```
You can trigger a request explicitly by calling `generateRequest` on the element.

This component manages the native header
## Dependencies

The component depends on the native plugin, you could see the url on the bower file.

Example:
```html
<cells-cordova-nativeheader></cells-cordova-nativeheader>
```

## Methods

#### addHeader
This method shows a native header and add a title

```js
component.addHeader({"title":"Bienvenido a BBVA"});
```

#### hideHeader
This method hide the native header

```js
component.hideHeader();
```

#### showHeader
This method shows the native header

```js
component.showHeader();
```

#### changeTitle
This method change the native header title

```js
component.changeTitle({"title":"Bienvenido a BBVA"});
```

#### showBackButton
This method shows the back button of the native header

```js
component.showBackButton();
```

#### showMenuButton
This method shows the menu button of the native header

```js
component.showMenuButton();
```

#### hideBackButton
This method hide the back button of the native header

```js
component.hideBackButton();
```


#### hideMenuButton
This method hide the menu button of the native header

```js
component.hideMenuButton();
```

### ReceiveMessage
You will receive the message action like the example below.
Actions are:

- ``onBackClicked``
- ``onMenuClicked``
- ``onBackground``
- ``onForeground``

```js
receiveMessage: function(e) {
        console.log(e.origin);
        var data = JSON.parse(e.data);
        var pluginName = data.pluginName;
        var action = data.action;
        var origin = data.origin;
        console.log(e.data);
        document.getElementById("data").innerHTML = ";pluginName: "+pluginName+
        " ;action: "+action+" ;origin: "+origin;
    }
```