/**
 * `CellsBehaviors.CellsTemplateAnimationBehavior` is a behavior to manage template animations (transitions between pages).
 *
 * #### Import
 *
 * 1) Import the behavior in your component:
 *
 * ```html
 * <link rel="import" href="../cells-template-animation-behavior/cells-template-animation-behavior.html">
 * ```
 *
 * 2) Add Polymer.CellsTemplateAnimationBehavior to the behaviors list in the JS file or script of your component:
 *
 * ```js
 *     class XCellsTemplateAnimationBehavior extends CellsBehaviors.CellsTemplateAnimationBehavior(Polymer.Element) {
 *       static get is() {
 *         return 'x-cells-amount-behavior-test';
 *       }
 *
 *       static get properties() {
 *         return {};
 *       }
 *     }
 *
 *     customElements.define(XCellsTemplateAnimationBehavior.is, XCellsTemplateAnimationBehavior);
 * ```
 *
 * 3) Add `cells-template-animation-behavior-styles` in the style tag of your component:
 *
 * ```html
 * <style include="name-your-component-styles cells-template-animation-behavior-styles"></style>
 * ```
 *
 * ## Available animation types
 * The animation type is established setting different values for the `animation-type` attribute for the outgoing and incoming pages.
 *
 * ### `horizontal` (default)
 * Both pages have a horizontal transition in both directions (forwards and backwards).
 *
 * ![horizontal animation](docs/horizontal.png)
 *
 * ### `verticalDownForwards`
 * The outgoing page has a vertical transition (to bottom) in the forwards direction and a horizontal transition (to the right) in the backwards direction.
 *
 * **animation-type**
 * - outgoing page: `verticalDownForwards`
 * - incoming page: `horizontal`
 *
 * ![verticalDownForwards animation](docs/verticalDownForwards.png)
 *
 * ### `verticalDownBackwards`
 * The outgoing page has a horizontal transition (to the left) in the forwards direction and a vertical transition (to bottom) in the backwards direction.
 *
 * **animation-type**
 * - outgoing page: `horizontal`
 * - incoming page: `verticalDownBackwards`
 *
 * ![verticalDownBackwards animation](docs/verticalDownBackwards.png)
 *
 * ### `verticalUp` (modal behavior)
 * The outgoing page remains static while the new page appears over it with a vertical transition (from bottom to top) in the forwards direction. The outgoing page disappears with a vertical transition (to bottom) revealing the previous page below it.
 *
 * **animation-type**
 * - outgoing page: `static`
 * - incoming page: `verticalUp`
 *
 * ![verticalUp animation](docs/verticalUp.png)
 *
 * ### `staticEver` (no animation)
 * There is no visible animation between pages.
 *
 * **animation-type**
 * - outgoing page: `staticEver`
 * - incoming page: `staticEver`
 *
 * ![staticEver animation](docs/staticEver.png)
 *
 * ## Reset scroll in cached template
 *
 * To reset the scroll in a template after navigating to another page, set the property `resetScroll` to `true` in the template you want to reset. By default, the scroll is reset in a node with ID `app__main`. If your template does not have that node, you should specify the node in which the scroll will be reset using the property `scrollerNode`. This attribute expects a CSS selector like `#app__main`.
 *
 * Example (json or js in app config):
 *
 * ```js
 * template: {
 *   tag: 'cells-template-1-column-right-sidebar',
 *   properties: {
 *     animationType: 'verticalUp',
 *     resetScroll: true,
 *     scrollerNode: '#some-node'
 *   }
 * }
 * ```
 * ## Styling
 * The following custom properties and mixins are available for styling:
 *
 * ### Custom Properties
 * | Custom Property                            | Selector                          | CSS Property                      | Value                         |
 * | ------------------------------------------ | --------------------------------- | --------------------------------- | ----------------------------- |
 * | --cells-template-animation-duration        | :host(.template-animation-static) | -webkit-animation-duration        |  230ms                        |
 * | --cells-template-animation-duration        | :host(.template-animation-static) | animation-duration                |  230ms                        |
 * | --cells-template-animation-timing-function | :host(.template-animation-static) | -webkit-animation-timing-function |  cubic-bezier(0.4, 0, 0.2, 1) |
 * | --cells-template-animation-timing-function | :host(.template-animation-static) | animation-timing-function         |  cubic-bezier(0.4, 0, 0.2, 1) |
 *
 * ### @apply
 * | Mixins                     | Selector | Value |
 * | -------------------------- | -------- | ----- |
 * | --cells-template-animation | :host    | {}    |
 *
 * @polymer
 * @customElement
 * @summary Behavior to manage template animations.
 * @extends {superClass}
 * @demo demo/index.html
 * @hero cells-template-animation-behavior.jpg
 */
window.CellsBehaviors = window.CellsBehaviors || {};
CellsBehaviors.CellsTemplateAnimationBehavior = (superClass) => class extends superClass {
  static get properties() {
    return {
      /**
       * Template name.
       */
      name: {
        type: String,
      },

      /**
       * Can be 'cached', 'inactive', 'active'.
       */
      state: {
        type: String,
        reflectToAttribute: true,
        observer: '_stateChanged',
      },

      /**
       * Name of the event that will be fired when the animation ends.
       */
      animationCompleteEvent: {
        type: String,
        value: 'template-active',
      },

      /**
       * Can be 'horizontal' (default), 'horizontalEver', 'staticEver', 'static', 'verticalDownForwards', 'verticalDownBackwards' or 'verticalUp'.
       */
      animationType: {
        type: String,
        value: 'horizontal',
        notify: true,
      },

      /**
       * An object that configurates the class name used for each direction for the 'horizontal' animationType.
       */
      horizontal: {
        type: Object,
        value: {
          forwardsIn: 'template-animation-moveFromRight',
          forwardsOut: 'template-animation-moveToLeft',
          backwardsIn: 'template-animation-moveFromLeft',
          backwardsOut: 'template-animation-moveToRight',
        },
      },

      /**
       * An object that configurates the class name used for each direction for the 'horizontalEverForwards' animationType.
       */
      horizontalEverForwards: {
        type: Object,
        value: {
          forwardsIn: 'template-animation-moveFromRight',
          forwardsOut: 'template-animation-moveToLeft',
          backwardsIn: 'template-animation-moveFromRight',
          backwardsOut: 'template-animation-moveToLeft',
        },
      },

      /**
       * An object that configurates the class name used for each direction for the 'horizontalEverBackwards' animationType.
       */
      horizontalEverBackwards: {
        type: Object,
        value: {
          forwardsIn: 'template-animation-moveFromLeft',
          forwardsOut: 'template-animation-moveToRight',
          backwardsIn: 'template-animation-moveFromLeft',
          backwardsOut: 'template-animation-moveToRight',
        },
      },

      /**
       * An object that configurates the class name used for each direction for the 'verticalDownForwards' animationType.
       */
      verticalDownForwards: {
        type: Object,
        value: {
          forwardsIn: 'template-animation-static',
          forwardsOut: 'template-animation-moveToBottom',
          backwardsIn: 'template-animation-moveFromLeft',
          backwardsOut: 'template-animation-moveToRight',
        },
      },

      /**
       * An object that configurates the class name used for each direction for the 'verticalDownBackwards' animationType.
       */
      verticalDownBackwards: {
        type: Object,
        value: {
          forwardsIn: 'template-animation-moveFromRight',
          forwardsOut: 'template-animation-moveToLeft',
          backwardsIn: 'template-animation-static',
          backwardsOut: 'template-animation-moveToBottom',
        },
      },

      /**
       * An object that configurates the class name used for each direction for the 'verticalUp' animationType.
       */
      verticalUp: {
        type: Object,
        value: {
          forwardsIn: 'template-animation-moveFromBottom',
          forwardsOut: 'template-animation-static',
          backwardsIn: 'template-animation-static',
          backwardsOut: 'template-animation-moveToBottom',
        },
      },

      /**
       * An object that configurates the class name used for each direction for the 'verticalUpForwards' animationType.
       */
      verticalUpForwards: {
        type: Object,
        value: {
          forwardsIn: 'template-animation-moveFromBottom',
          forwardsOut: 'template-animation-static',
          backwardsIn: 'template-animation-moveFromLeft',
          backwardsOut: 'template-animation-moveToRight',
        },
      },

      /**
       * An object that configurates the class name used for each direction for the 'static' animationType.
       */
      static: {
        type: Object,
        value: {
          forwardsIn: 'template-animation-static',
          forwardsOut: 'template-animation-static',
          backwardsIn: 'template-animation-static',
          backwardsOut: 'template-animation-static',
        },
      },

      /**
       * An object that configurates the animation name .class type for fade
       */
      fade: {
        type: Object,
        value: {
          forwardsIn: 'template-animation-fade-in',
          forwardsOut: 'template-animation-fade-out',
          backwardsIn: 'template-animation-fade-in',
          backwardsOut: 'template-animation-fade-out',
        },
      },
    };
  }

  constructor() {
    super();
    this._resetTemplate = this._resetTemplate.bind(this);
  }

  ready() {
    this.setAttribute('aria-hidden', 'true');
    super.ready();
  }

  /**
   * Get current HTMLElement template based on given state.
   *
   * @private
   * @method _findTemplateByState
   * @param  {String}       state   Target template state.
   * @return {HTMLElement}          Template that matches given state.
   */
  _findTemplateByState(state) {
    const isContainedOnPage = !!this.parentNode.host;
    const currentPage = !isContainedOnPage ? this : this.parentNode.host;
    const pagesContainer = currentPage.parentElement;
    const allPages = pagesContainer.childNodes;
    const allSiblingsPages = Array.from(allPages).filter(page => page !== currentPage && this._isValidTarget(page));
    let targetTemplate;

    allSiblingsPages.some(page => {
      const templatePage = this._isPage(page) ? this._getCellsTemplateFromShadowRootChildNodes(page) : page;
      const isTarget = templatePage.getAttribute('state') === state;

      if (isTarget) {
        targetTemplate = templatePage;
      }

      return isTarget;
    });

    return targetTemplate;
  }

  /**
   * Checks if given html element is valid target.
   *
   * @private
   * @method _isValidTarget
   * @param  {HTMLElement}       node   Target HTML element.
   * @return {Boolean}
   */
  _isValidTarget(node) {
    const TARGETS = ['template', 'page'];

    return node && node.tagName && TARGETS.some(target => node.tagName.toLowerCase().indexOf(target) !== -1);
  }

  /**
   * Checks if given html element is a page component.
   *
   * @private
   * @method _isPage
   * @param  {HTMLElement}       node   Target HTML element.
   * @return {Boolean}
   */
  _isPage(node) {
    return node && node.tagName.toLowerCase().endsWith('-page');
  }

  /**
   * Returns first element from shadowRoot child nodes that matchs 'cells-template' tagname.
   *
   * @private
   * @method _getCellsTemplateFromShadowRootChildNodes
   * @param  {HTMLElement} node First level component that contains cells-template inside shadowRoot childNodes.
   * @return {HTMLElement}      Cells template.
   */
  _getCellsTemplateFromShadowRootChildNodes(node) {
    return Array.from(node.shadowRoot.childNodes).find(el => el && el.tagName && el.tagName.toLowerCase().indexOf('cells-template') !== -1);
  }

  /**
   * Fired when template animation ends.
   * @event this.animationCompleteEvent ('default value: template-active')
   */

  /**
   * state template ('cached', 'inactive', 'active', 'native')
   * @param {string} newState
   */
  _stateChanged(newState) {
    if (newState === 'native') {
      this._removeStateVisibleTemplateOut(this);
      return;
    }

    if (newState === 'active') {
      let inactiveTemplate = this._findTemplateByState('inactive');
      if (inactiveTemplate) {
        this._animateWith(inactiveTemplate);
      } else {
        this.classList.add('state-is-visible');
        this.setAttribute('aria-hidden', 'false');
        this.dispatchEvent(new CustomEvent(this.animationCompleteEvent, {
          bubbles: true,
          composed: true,
        }));
      }
    }
  }

  /**
   * Fired from outgoing template when forward animation.
   * @event 'animation-forward'
   */
  _directAnimationForwards(templateIn, templateOut, animationType) {
    templateIn.classList.add(animationType.forwardsIn);
    templateOut.classList.add(animationType.forwardsOut);

    templateOut.dispatchEvent(new CustomEvent('animation-forward', {
      bubbles: true,
      composed: true,
    }));
  }

  /**
   * Fired from outgoing template when backward animation.
   * @event 'animation-backward'
   */
  _directAnimationBackwards(templateIn, templateOut, animationType) {
    templateIn.classList.add(animationType.backwardsIn);
    templateOut.classList.add(animationType.backwardsOut);

    templateOut.dispatchEvent(new CustomEvent('animation-backward', {
      bubbles: true,
      composed: true,
    }));
  }

  _configureAnimationTypeActive(templateB) {
    let animationTypeIn = this.animationType;
    let animationTypeOut = templateB.animationType;

    let map = {
      'verticalDownBackwards': 'verticalDownBackwards',
      'verticalDownForwards': 'verticalDownForwards',
      'verticalUp': 'verticalUp',
      'verticalUpForwards': 'verticalUpForwards',
      'staticEver': 'static',
      'horizontalEver': 'horizontal',
      'fade': 'fade',
    };

    if (animationTypeOut === animationTypeIn) {
      [animationTypeOut, animationTypeIn].forEach(anim => {
        return (anim in map) ? this[ map[ anim ] ] : '';
      });
    }

    if (animationTypeIn === 'horizontalEver' && animationTypeOut === 'static') {
      return this.horizontalEverForwards;
    }

    if (animationTypeOut === 'horizontalEver' && animationTypeIn === 'static') {
      return this.horizontalEverBackwards;
    }

    if (map[ animationTypeOut ]) {
      return this[ map[ animationTypeOut ] ];
    }

    if (map[ animationTypeIn ]) {
      return this[ map[ animationTypeIn ] ];
    }

    return this.horizontal;
  }

  _cleanTemplateNextNavigation(template) {
    if (template) {
      let templateOut = template._nextNavigation;
      template._nextNavigation = undefined;
      this._cleanTemplateNextNavigation(templateOut);
    }
  }

  _animateWith(templateOut) {
    let reverse = false;
    let animationEndEvent = ('AnimationEvent' in window) ? 'animationend' : 'webkitAnimationEnd';
    let animationTypeActive;

    if (this._nextNavigation) {
      this._cleanTemplateNextNavigation(this);
      reverse = true;
    } else {
      templateOut._nextNavigation = this;
    }

    this.addEventListener(animationEndEvent, this._resetTemplate);
    templateOut.addEventListener(animationEndEvent, this._resetTemplate);

    this.classList.add('template-animation-is-animating');
    templateOut.classList.add('template-animation-is-animating');
    animationTypeActive = this._configureAnimationTypeActive(templateOut);
    window.requestAnimationFrame(() => {
      if (!reverse) {
        this._directAnimationForwards(this, templateOut, animationTypeActive);
      } else {
        this._directAnimationBackwards(this, templateOut, animationTypeActive);
      }
      this.setAttribute('aria-hidden', 'false');
      this.classList.add('state-is-visible');
    });
  }

  _resetTemplate(evt) {
    let AT_TARGET = Event.AT_TARGET || Event.prototype.AT_TARGET;
    if (evt.eventPhase !== AT_TARGET) {
      return;
    }

    let node = evt.target;
    let regClsTemplate = /\btemplate-animation\S+/g;
    node.className = (node.className || '').replace(regClsTemplate, '');
    node.removeEventListener(evt.type, this._resetTemplate);

    if (node !== this) {
      this._removeStateVisibleTemplateOut(node);
    }
  }

  _removeStateVisibleTemplateOut(templateOut) {
    if (!templateOut.disabledScrollerReset && templateOut._scrollerNode) {
      templateOut._scrollerNode.scrollTop = 0;
    }
    window.requestAnimationFrame(() => {
      templateOut.setAttribute('aria-hidden', 'true');
      templateOut.classList.remove('state-is-visible');
      this.dispatchEvent(new CustomEvent(this.animationCompleteEvent, {
        bubbles: true,
        composed: true,
      }));
    });
  }
};
