![cells-demo-dm-helper screenshot](cells-demo-dm-helper.png)

# cells-demo-dm-helper

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

[Demo of component in Cells Catalog](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html#/elements/#cells-demo-dm-helper)

`<cells-demo-dm-helper>` is a helper component to showcase Data Managers (DM).

It expects an array of DM methods that will be printed as buttons in the top bar. Each item of the array is an Object that must provide the text for the button (`label`) and/or the method name (`method`) and the name of the event (`event`) that will be fired after clicking that button.

The component's user is responsible of listen to that event and make the call to the corresponding method on the DM.

To display a loading icon after clicking a button, set `loadingData` to `true`.

Example:

```html
<cells-demo-dm-helper
  loading-data="[[loading]]"
  on-my-custom-event="callDmMethod"
  dm-methods='[{
    "label": "Get Something",
    "method": "getSomething()",
    "event": "my-custom-event"
  }]'></cells-demo-dm-helper>

<my-datamanager id="dm"></my-datamanager>
```

```js
callDmMethod = function() {
  domBind.loading = true;
  domBind.$.dm.getSomething();
}
```

## Content hooks

The component has two content hooks, one for the reference component that uses the DM and other for additional elements in the top bar.

Example:

```html
<cells-demo-dm-helper>
  <input type="text" slot="top-bar" />
  <reference-component slot="component"></reference-component>
</cells-demo-dm-helper>
```


## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-demo-dm-helper  | Mixin applied to :host     | {}  |
| --cells-fontDefault  | Font family of :host    | sans-serif  |
| --cells-demo-dm-helper-code-font-family | Font family for `<code>` used in buttons | roboto mono, monaco, monospace |
| --cells-demo-dm-helper-wrapper | Mixin applied to the wrapper | {} |
| --cells-demo-dm-helper-actions-bar | Mixin applied to the top actions bar | {} |
| --cells-demo-dm-helper-actions-bar-left | Mixin applied to the left child of the actions bar | {} |
| --cells-demo-dm-helper-actions-bar-tablet | Mixin applied to the left child of the actions bar (>= 768px) | {} |
| --cells-demo-dm-helper-actions-bar-left-tablet | Mixin applied to the left child of the actions bar (>= 768px) | {} |
| --cells-demo-dm-helper-login-status | Mixin applied to the login status wrapper | {} |
| --cells-demo-dm-helper-login-status-indicator | Mixin applied to the login status indicator text | {} |
| --cells-demo-dm-helper-indicator-color-warning | Color of the "logging in" ball | #F7893B |
| --cells-demo-dm-helper-indicator-color-ok | Color of the "login success" ball | #388d4f |
| --cells-demo-dm-helper-indicator-color-ko | Color of the "login error" ball | #b92a45 |
| --cells-demo-dm-helper-indicator-ball | Mixin applied to the login ball indicator | {} |
| --cells-demo-dm-helper-resizable-panels-knob | Mixin applied to the knob in the resizable panels | {} |
| --cells-demo-dm-helper-panel-left | Mixin applied to the left panel | {} |
| --cells-demo-dm-helper-panel-right | Mixin applied to the right panel | {} |
| --cells-demo-dm-helper-loader-icon | Mixin applied to the loader icon | {} |
