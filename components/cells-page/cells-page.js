{
  const CellsElement = customElements.get('cells-element');
  const BRIDGE_PAGE_PRIVATE_CHANNEL_PREFIX = '__bridge_page_';
  const getPagePrivateChannel = tagName => `${BRIDGE_PAGE_PRIVATE_CHANNEL_PREFIX}${tagName.toLowerCase().replace('-page', '')}`;

  class CellsPage extends CellsElement {
    static get is() {
      return 'cells-page';
    }

    static get properties() {
      return {
        params: {
          type: Object,
          value: {}
        },
        num = Number
      };
    }

    ready() {
      super.ready();

      this.__handleConnections();
    }

    __handleConnections() {
      if (this.__hasPageHandlers()) {
        this.__handlePagePrivateChannel();
      }
    }

    __hasPageHandlers() {
      return !!this.onPageEnter || !!this.onPageLeave;
    }

    __handlePagePrivateChannel() {
      const channelName = getPagePrivateChannel(this.tagName);
      const wrappedPrivateChannelCallback = this.__wrapPrivateChannelCallback();

      this.subscribe(channelName, wrappedPrivateChannelCallback);
    }

    __wrapPrivateChannelCallback() {
      return ({value: pageStatusValue}) => {
        const callback = () => pageStatusValue ? this.onPageEnter && this.onPageEnter() : this.onPageLeave && this.onPageLeave();

        if (callback) {
          callback();
        }
      };
    }
  }

  customElements.define(CellsPage.is, CellsPage);
}
