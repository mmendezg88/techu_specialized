{
  const DEBUG = false;
  const _log = message => DEBUG && console.log(message);
  const callCellsBridge = (args) => {
    const [command, ...parameters] = args;

    // cells is ready
    if (window.cells) {
      if (!window.cells[command]) {
        throw new Error(`WARNING: Invalid cells bridge command execution: ${command}.`);
      }

      const result = window.cells[command](...parameters);
      _log(`Executing bridge command: ${command}.`);
      return result;
    }

    window.cellsBridgeQueue = window.cellsBridgeQueue || [];
    window.cellsBridgeQueue.push({ command, parameters });
    _log(`Pushing bridge command execution to queue: ${command}.`);
  };

  class CellsElement extends Polymer.Element {
    static get is() {
      return 'cells-element';
    }

    static get properties() {
      return {

      };
    }

    subscribe(channelName, callback) {
      this.__callCellsBridge('registerInConnection', channelName, this, callback);
    }

    unsubscribe(channels) {
      this.__callCellsBridge('unsubscribe', channels, this);
    }

    publish(channelName, value, options = {}) {
      this.__callCellsBridge('publish', channelName, value, options);
    }

    publishOn(channelName, htmlElement, eventName) {
      this.__callCellsBridge('registerOutConnection', channelName, htmlElement, eventName);
    }

    navigate(page, params) {
      this.__callCellsBridge('navigate', page, params);
    }

    // native
    navigateToNative(page, params) {
      this.__callCellsBridge('navigateToNative', page, params);
    }

    backStep() {
      this.__callCellsBridge('backStep');
    }

    // monitoring
    log(log) {
      this.__callCellsBridge('log', log);
    }

    ingest(spans) {
      this.__callCellsBridge('ingest', spans);
    }

    createSpan(data) {
      return this.__callCellsBridge('createSpan', data);
    }

    createUUID() {
      return this.__callCellsBridge('createUUID');
    }

    // wrapper just for testing purposes
    __callCellsBridge(...args) {
      return callCellsBridge(args);
    }
  }

  customElements.define(CellsElement.is, CellsElement);
}
