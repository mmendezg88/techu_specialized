# &lt;cells-element&gt;

Your component description.

![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)
Example:
```html
<cells-element></cells-element>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:---------------|
| --cells-element  | Mixin applied to :host     | {}  |
| --cells-fontDefault  | Mixin applied to :host font-family    | sans-serif  |
