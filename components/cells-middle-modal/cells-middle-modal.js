/**
 * # cells-middle-modal
 *
 * `cells-middle-modal` shows a modal window aligned at the center of the screen.
 *
 * ## Try it
 *
 * __Simple example__
 * ```html
 * <cells-middle-modal id='modal-example' aria-label="Lorem ipsum">
 * Lorem-ipsum
 * </cells-middle-modal>
 * ```
 *
 * __Complex example__
 * ```html
 * <cells-middle-modal id='modal-example' aria-labelledby="modal-aria-label" click-on-close close-icon='coronita:close' focus-target="btn1">
 * <div class="header" id="modal-aria-label"> My Header </div>
 * <div class="body"> My Body </div>
 * <div class="buttons">
 * <button id="btn1"/>
 * <button id="btn2"/>
 * </div>
 * </cells-middle-modal>
 * ```
 *
 * You can use helper classes inside the component content:
 * - __header__  sets default color and size for headers
 * - __body__ sets default padding and margin for bodies
 * - __buttons__ aligns horizontally your buttons
 *
 * ## Customize it
 *
 * __Close on click in the overlay__
 * Use the property `close-on-click` to allow the modal to hide itself when the overlay is clicked
 *
 * __Close icon__
 * Use property `close-icon` to provide a close icon at the top-right of the modal
 *
 * __Header with background color__
 * Use property `main-header-text` to show a text as a header with colored background on the top of the modal
 *
 * __Place custom content before the dialog box__
 * Use the slot `pre-dialog`. Example:
 *
 * ```html
 * <cells-middle-modal>
 * <button slot="pre-dialog">Close</button>
 * </cells-middle-modal>
 * ```
 *
 * ## Handle it
 *
 * Set the boolean property `open` to true or false to open or close the modal.
 *
 * Buttons inside light DOM with attribute `data-action="hide"` also close the modal by setting `open` to false.
 *
 * ```html
 * <cells-middle-modal>
 * <button data-action="hide">Close</button>
 * </cells-middle-modal>
 * ```
 *
 * ## Accessibility
 *
 * The component applies a "dialog" aria role to the modal by default. You can pass a `role` attribute to change it to "alertdialog" if you need it.
 *
 * You __must__ provide either an `aria-label` or `aria-labelledby` attribute to the component, as well as an `aria-describedby` attribute if necessary.
 *
 * An open dialog can be closed using ESC key.
 *
 * Also, an open dialog traps focus inside it. `focus-target` attribute should be used to pass the Id of the modal content element which should receive focus when the modal is opened. If `close-icon` is provided and `focus-target` is not, focus will be set to the close icon.
 *
 * When using `main-header-text` attribute, its text will receive a `role=heading` attribute and an `aria-level=1` attribute. You can set a different aria-level using `heading-level` attribute and passing a number to it. In case you don't want that text to behave as a heading, role and aria-level will be entirely removed passing a 0 value to `heading-level`.
 *
 * Examples of accessible dialogs:
 *
 * __Accessible dialog modal__
 * ```html
 * <cells-middle-modal aria-labelledby="aria-text" close-icon="coronita:GM02" main-header-text="Help" heading-level="2">
 * <div class="header" id="aria-text">Action result</div>
 * <p class="body">Thank you for your message. You'll receive a confirmation in your mail address soon.</p>
 * </cells-middle-modal>
 * ```
 *
 * __Accessible alertdialog modal__
 * ```html
 * <cells-middle-modal role="alertdialog" aria-label="Action confirmation" aria-describedby="aria-text" focus-target="btn-no">
 * <p class="body" id="aria-text">Are you sure you want to execute this action?</p>
 * <div class="buttons">
 * <ul>
 * <li><button type='button' id="btn-no">No</button></li>
 * <li><button type='button'>Yes</button></li>
 * </ul>
 * </div>
 * </cells-middle-modal>
 * ```
 *
 * ## Icons
 *
 * Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html). In fact, this component uses an iconset in its demo.
 *
 * ## Styling
 *
 * The following custom properties and mixins are available for styling:
 *
 * ### Custom Properties
 * | Custom Property                                                   | Selector                                                    | CSS Property     | Value                                                             |
 * | ----------------------------------------------------------------- | ----------------------------------------------------------- | ---------------- | ----------------------------------------------------------------- |
 * | --cells-fontDefault                                               | :host                                                       | font-family      |  sans-serif                                                       |
 * | --cells-middle-modal-md-modal-max-width                           | .md-modal                                                   | max-width        |  71.25rem                                                         |
 * | --cells-middle-modal-md-modal-min-width                           | .md-modal                                                   | min-width        |  18.75rem                                                         |
 * | --cells-middle-modal-md-overlay-background-color                  | .md-overlay                                                 | background-color |  ![#121212](https://placehold.it/15/121212/000000?text=+) #121212 |
 * | --cells-middle-modal-md-overlay-opacity                           | :host([open]) > .md-overlay                                 | opacity          |  0.6                                                              |
 * | --cells-middle-modal-md-wrapper-background-color                  | .md-wrapper                                                 | background       |  ![#fff](https://placehold.it/15/fff/000000?text=+) #fff          |
 * | --cells-middle-modal-md-wrapper-color                             | .md-wrapper                                                 | color            |  ![#666](https://placehold.it/15/666/000000?text=+) #666          |
 * | --cells-middle-modal-md-wrapper-border-radius                     | .md-wrapper                                                 | border-radius    |  1px                                                              |
 * | --cells-middle-modal-md-content-padding                           | .md-content                                                 | padding          |  1.25em 1em 1.875em                                               |
 * | --cells-middle-modal-md-content__btn-close-horizontal             | .md-content__btn-close                                      | left             |  1em                                                              |
 * | --cells-middle-modal-md-content__btn-close-top                    | .md-content__btn-close                                      | top              |  1.25em                                                           |
 * | --cells-middle-modal-close-icon-color                             | .md-content__btn-close .icon-close                          | color            |  ![#2a86ca](https://placehold.it/15/2a86ca/000000?text=+) #2a86ca |
 * | --cells-middle-modal-with-close-md-content-padding                | :host([close-icon]) .md-content                             | padding          |  4.75em 1em 1.875em                                               |
 * | --cells-middle-modal-main-header-color                            | .main-header-text .main-header                              | color            |  ![#fff](https://placehold.it/15/fff/000000?text=+) #fff          |
 * | --cells-middle-modal-main-header-background-color                 | .main-header-text .main-header                              | background-color |  ![#d8be75](https://placehold.it/15/d8be75/000000?text=+) #d8be75 |
 * | --cells-middle-modal-main-header-text-size                        | .main-header-text .main-header #heading                     | font-size        |  1.5rem                                                           |
 * | --cells-middle-modal-main-header-md-content__btn-close-horizontal | .main-header-text .md-content__btn-close                    | right            |  1.5em                                                            |
 * | --cells-middle-modal-main-header-md-content__btn-close-top        | .main-header-text .md-content__btn-close                    | top              |  1.5em                                                            |
 * | --cells-middle-modal-main-header-text-close-icon-color            | .main-header-text .md-content__btn-close .icon-close        | color            |  ![#fff](https://placehold.it/15/fff/000000?text=+) #fff          |
 * | --cells-middle-modal-content-header-color                         | .main-header-text .md-content ::slotted(.header)            | color            |  ![#121212](https://placehold.it/15/121212/000000?text=+) #121212 |
 * | --cells-middle-modal-content-header-text-size                     | .main-header-text .md-content ::slotted(.header)            | font-size        |  1.5rem                                                           |
 * | --cells-middle-modal-content-body-color                           | .main-header-text .md-content ::slotted(.body)              | color            |  ![#121212](https://placehold.it/15/121212/000000?text=+) #121212 |
 * | --cells-middle-modal-content-body-text-size                       | .main-header-text .md-content ::slotted(.body)              | font-size        |  0.8125rem                                                        |
 * | --cells-middle-modal-content-body-line-height                     | .main-header-text .md-content ::slotted(.body)              | line-height      |  1.125rem                                                         |
 * | --cells-middle-modal-desktop-md-content-max-width                 | @media (min-width: 768px) > .md-content                     | max-width        |  34.375rem                                                        |
 * | --cells-middle-modal-md-content-padding                           | @media (min-width: 768px) > .md-content                     | padding          |  5.625em 0 5em                                                    |
 * | --cells-middle-modal-md-content__btn-close-horizontal             | @media (min-width: 768px) > .md-content__btn-close          | right            |  1.5625rem                                                        |
 * | --cells-middle-modal-md-content__btn-close-top                    | @media (min-width: 768px) > .md-content__btn-close          | top              |  1.5625rem                                                        |
 * | --cells-middle-modal-content-header-text-size                     | @media (min-width: 768px) > .md-content ::slotted(.header)  | font-size        |  2.25rem                                                          |
 * | --cells-middle-modal-content-header-color                         | @media (min-width: 768px) > .md-content ::slotted(.body)    | color            |  ![#666](https://placehold.it/15/666/000000?text=+) #666          |
 * | --cells-middle-modal-content-body-text-size                       | @media (min-width: 768px) > .md-content ::slotted(.body)    | font-size        |  0.9375rem                                                        |
 * | --cells-middle-modal-content-body-line-height                     | @media (min-width: 768px) > .md-content ::slotted(.body)    | line-height      |  1.4375rem                                                        |
 * | --cells-middle-modal-md-content-padding                           | @media (min-width: 768px) > :host([close-icon]) .md-content | padding          |  5.625em 0 5em                                                    |
 * ### @apply
 * | Mixins                                                       | Selector                                                    | Value |
 * | ------------------------------------------------------------ | ----------------------------------------------------------- | ----- |
 * | --cells-middle-modal                                         | :host                                                       | {}    |
 * | --cells-middle-modal-md-modal                                | .md-modal                                                   | {}    |
 * | --cells-middle-modal-open                                    | :host([open]) > .md-modal                                   | {}    |
 * | --cells-middle-modal-property-open                           | :host([open])                                               | {}    |
 * | --cells-middle-modal-md-overlay                              | .md-overlay                                                 | {}    |
 * | --cells-middle-modal-md-overlay-open                         | :host([open]) > .md-overlay                                 | {}    |
 * | --cells-middle-modal-md-wrapper                              | .md-wrapper                                                 | {}    |
 * | --cells-middle-modal-md-content                              | .md-content                                                 | {}    |
 * | --cells-middle-modal-md-content__btn-close                   | .md-content__btn-close                                      | {}    |
 * | --cells-middle-modal-md-content--icon-close                  | .md-content__btn-close .icon-close                          | {}    |
 * | --cells-middle-modal-with-close-md-content                   | :host([close-icon]) .md-content                             | {}    |
 * | --cells-middle-modal-main-header                             | .main-header-text .main-header                              | {}    |
 * | --cells-middle-modal-main-header-heading                     | .main-header-text .main-header #heading                     | {}    |
 * | --cells-middle-modal-main-header-with-close-icon             | .main-header-text.with-close-icon .main-header              | {}    |
 * | --cells-middle-modal-main-header-text-md-content             | .main-header-text .md-content                               | {}    |
 * | --cells-middle-modal-main-header-text-md-content__btn-close  | .main-header-text .md-content__btn-close                    | {}    |
 * | --cells-middle-modal-main-header-text-md-content--icon-close | .main-header-text .md-content__btn-close .icon-close        | {}    |
 * | --cells-middle-modal-md-content--header                      | .main-header-text .md-content ::slotted(.header)            | {}    |
 * | --cells-middle-modal-md-content--body                        | .main-header-text .md-content ::slotted(.body)              | {}    |
 * | --cells-middle-modal-md-content--buttons                     | .main-header-text .md-content ::slotted(.buttons)           | {}    |
 * | --cells-middle-modal-md-effect                               | .md-effect .md-wrapper                                      | {}    |
 * | --cells-middle-modal-md-effect--open                         | :host([open]) .md-effect .md-wrapper                        | {}    |
 * | --cells-middle-modal-desktop-md-content                      | @media (min-width: 768px) > .md-content                     | {}    |
 * | --cells-middle-modal-desktop-md-content__btn-close           | @media (min-width: 768px) > .md-content__btn-close          | {}    |
 * | --cells-middle-modal-desktop-md-content--header              | @media (min-width: 768px) > .md-content ::slotted(.header)  | {}    |
 * | --cells-middle-modal-desktop-md-content--body                | @media (min-width: 768px) > .md-content ::slotted(.body)    | {}    |
 * | --cells-middle-modal-desktop-md-content--buttons             | @media (min-width: 768px) > .md-content ::slotted(.buttons) | {}    |
 * | --cells-middle-modal-desktop-with-close-md-content           | @media (min-width: 768px) > :host([close-icon]) .md-content | {}    |
 *
 * @polymer
 * @customElement
 * @extends {Polymer.Element}
 * @demo demo/index.html
 * @hero cells-middle-modal.png
 */
class CellsMiddleModal extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior ], Polymer.Element) {
  static get is() {
    return 'cells-middle-modal';
  }

  static get properties() {
    return {
      /**
       * Allows the modal to hide itself when the overlay is clicked
       */
      closeOnClick: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
      /**
       * Allows adding an icon to close the modal. If no focusTarget is defined, this icon will receive the focus each time the modal is opened.
       */
      closeIcon: {
        type: String,
      },
      /**
       * Allows adding a header with an icon to close the modal. If no focusTarget is defined, this icon will receive the focus each time the modal is opened.
       */
      mainHeaderText: {
        type: String,
      },
      /**
       * Allows to define an aria-level for the main header text. If 0 is provided, main header text won't be treated as a heading.
       */
      headingLevel: {
        type: Number,
        value: 1,
      },
      /**
       * If true, makes modal visible
       */
      open: {
        type: Boolean,
        observer: '_open',
        reflectToAttribute: true,
        notify: true,
      },
      /**
       * Set the Id of one of the modal contents here to automatically move the focus to that element (if it's focusable) each time the modal is opened.
       */
      focusTarget: {
        type: String,
      },
      /**
       * Disable the use of the esc key to close the modal.
       */
      noCloseOnEscKey: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      },

      /**
       * Property to disable the focus back to the origin
       */
      noFocusOrigin: {
        type: Boolean,
        value: false
      }
    };
  }

  created() {
    this._onKeyDown = this._onKeyDown.bind(this);
  }

  ready() {
    super.ready();
    this._ensureAttribute('aria-hidden', true);
    this._ensureAttribute('role', 'dialog');
  }

  connectedCallback() {
    super.connectedCallback();
    this.addEventListener('click', this._clickHandler.bind(this));
    this.addEventListener('keydown', event => this._manageTabFocusout(event));
    document.addEventListener('focus', this._focusTrap.bind(this));
    Polymer.RenderStatus.afterNextRender(this, () => {
      if (this.shadowRoot.querySelector('.md-content').offsetHeight > this.offsetHeight) {
        this.scrollTop = 0;
      }
    });
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    this.removeEventListener('click', this._clickHandler.bind(this));
    this.removeEventListener('keydown', this._manageTabFocusout.bind(this));
    document.removeEventListener('focus', this._focusTrap.bind(this));
  }
  /**
   * Returns a specific class name for the modal content div if a main header text exists
   * @param {String} mainHeaderText
   * @return {String} Class for style.
   */
  _mainHeaderClass(mainHeaderText) {
    return this.closeIcon ? 'main-header-text with-close-icon' : 'main-header-text';
  }
  /**
   * Manage focus trap
   * @param {FocusEvent} event
   */
  _focusTrap(event) {
    if (this.open && event.composedPath().indexOf(this) === -1) {
      event.stopPropagation();
      this.$.dialog.focus();
    }
  }
  /**
   * Manages opening and closing the modal
   * @param {Boolean} newValue
   */
  _open(newValue, previousValue) {
    if (newValue) {
      this._show();
    } else if (previousValue) {
      this._hide();
    }
  }
  /**
   * Returns current active element taking Shadow DOM in account
   * @return {HTMLElement}
   */
  _deepActiveElement() {
    let active = document.activeElement || document.body;
    while (active.shadowRoot && active.shadowRoot.activeElement) {
      active = active.shadowRoot.activeElement;
    }
    return !this.noFocusOrigin ? active : null;
  }
  /**
   * Show `cells-middle-modal`
   */
  _show() {
    let dialog = this.$.dialog;
    document.addEventListener('keydown', this._onKeyDown, true);
    this.set('_focusOrigin', this._deepActiveElement());
    this.setAttribute('aria-hidden', 'false');
    requestAnimationFrame(() => {
      requestAnimationFrame(() => {
        let target = this.querySelector('#' + this.focusTarget) || this.shadowRoot.querySelector('#' + this.focusTarget);
        if (this.focusTarget && !!target) {
          setTimeout(() => {
            target.focus();
          }, 20);
        } else if (this.closeIcon) {
          this.shadowRoot.querySelector('#btn-close').focus();
        } else {
          dialog.focus();
        }
        this.dispatchEvent(new CustomEvent('cells-middle-modal-opened', {
          bubbles: true,
          composed: true
        }));
      });
    });
  }
  /**
   * Close modal and unlisten keyDown event when Esc key is pressed and dialog is open
   * @param {KeyboardEvent} event
   */
  _onKeyDown(event) {
    if (this.open && event.keyCode === 27 && !this.noCloseOnEscKey) {
      this.open = false;
      document.removeEventListener('keydown', this._onKeyDown, true);
    }
  }
  /**
   * Hide `cells-middle-modal`
   */
  _hide() {
    this.setAttribute('aria-hidden', true);
    if (this._focusOrigin) {
      this._focusOrigin.focus();
    }
    this.dispatchEvent(new CustomEvent('cells-middle-modal-hidden', {
      bubbles: true,
      composed: true
    }));
  }
  /**
   * Hide `cells-middle-modal` if attribute `closeOnClick` is set to true
   */
  _onClick() {
    if (this.closeOnClick) {
      this.open = false;
    }
  }
  /**
   * Fires 'cells-middle-modal-click-close' event before hiding the modal.
   */
  _onClickClose() {
    /**
     * Fired when `cells-middle-modal` is closed using the close button.
     * @event cells-middle-modal-close
     */
    this.dispatchEvent(new CustomEvent('cells-middle-modal-click-close', {
      bubbles: true,
      composed: true
    }));
    this.open = false;
  }

  _clickHandler(e) {
    let target = e.target;
    if (target && (target.dataset.action === 'hide' || target.closest('[data-action="hide"]'))) {
      this.open = false;
    }
  }

  _manageTabFocusout(event) {
    if (this.open && event.keyCode === 9) {
      let target = event.composedPath()[0];
      let focusElements = this.querySelectorAll('a[href],area[href],input:not([disabled]),select:not([disabled]),textarea:not([disabled]),button:not([disabled]),[tabindex]:not([tabindex="-1"])');
      if (!event.shiftKey && target === focusElements[focusElements.length - 1]) {
        event.preventDefault();
        this.$.dialog.focus();
      } else if (event.shiftKey && target === focusElements[0]) {
        event.preventDefault();
        this.$.dialog.focus();
      } else if (event.shiftKey && target === this.$.dialog) {
        event.preventDefault();
        focusElements[focusElements.length - 1].focus();
      }
    }
  }
}

window.customElements.define(CellsMiddleModal.is, CellsMiddleModal);
