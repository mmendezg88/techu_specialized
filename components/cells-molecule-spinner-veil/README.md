![cells-molecule-spinner-veil screenshot](cells-molecule-spinner-veil.png)

![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg) ![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)

[Demo of component in Cells Catalog](https://catalogs.platform.bbva.com/cells)

# cells-molecule-spinner-veil

`<cells-molecule-spinner-veil>` displays a full screen veil over contents below it with a customizable spinner.
A custom spinner element can be provided in light DOM by using the slot `spinner`.

Example with custom spinner:

```html
<cells-molecule-spinner-veil>
  <my-custom-spinner slot="spinner"></my-custom-spinner>
</cells-molecule-spinner-veil>
```

## Opening and closing

The veil can be shown using the boolean attribute `open` or programmatically using its `show()` and `hide()` methods.

To keep track of the number of times that the veil has been opened in order to prevent closing it before all the opened veils have been closed, `show()` and `hide()` methods should be used instead of the `open` property.

The number of remaining threads is available in the `thread` property (`readOnly`).

## Styling

### Solid background

Use the class `solid` to display a solid background.

```html
<cells-molecule-spinner-veil class="solid">
</cells-molecule-spinner-veil>
```

### Fade effect

Use the class `fade` to show/hide the veil with a fade in/out effect.

```html
<cells-molecule-spinner-veil class="fade">
</cells-molecule-spinner-veil>
```

The following custom properties and mixins are available for styling:

| Custom Property | Description | Default |
| :-------------- | :---------- | :------ |
| --cells-molecule-spinner-veil-background-color | veil background color | rgba(255, 255, 255, .8) |
| --cells-molecule-spinner-veil | empty mixin for :host | {} |
| --cells-molecule-spinner-veil-is-visible | empty mixin for :host(.is-visible) | {} |
| --cells-molecule-spinner-veil-fade | empty mixin applied to :host(.fade) | {} |
| --cells-molecule-spinner-veil-fade-visible | empty mixin applied to :host(.fade) when it's visible  | {} |
| --cells-molecule-spinner-veil-main | empty mixin for .spinner | {} |
| --cells-molecule-spinner-veil-ball | empty mixin for .spinner .ball | {} |
| --cells-molecule-spinner-veil-ball-dark | empty mixin for .spinner .ball.dark | {} |
| --cells-molecule-spinner-veil-ball-light | empty mixin for .spinner .ball.light | {} |
