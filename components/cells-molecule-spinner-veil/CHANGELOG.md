# Changelog

## v12.0.0
Migration to Polymer 2

## v10.0.0

### New features

- Hybrid support
- Ability to use a custom spinner provided in light DOM (`slot name="spinner"`)
- Ability to use a solid background by adding the class `solid`
- Ability to use a fade in/out effect by adding the class `fade`
